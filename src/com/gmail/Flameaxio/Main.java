package com.gmail.Flameaxio;

import java.util.Arrays;

/**
 * Displays the results of {@link Util} methods.
 * @author Olexandr Voinalovych
 */
public class Main {

    public static void main(String[] args) {
        String decodedInput = "Abcdefghij";
        System.out.println("Encoded: " + Util.encode(decodedInput, 2));
        System.out.println("Not encoded: " + Util.encode(decodedInput, -1));
        System.out.println("Not encoded again: " + Util.encode("", 2) + "\n");
        String encodedInput = "dhaeibfjcg";
        System.out.println("Decoded: " + Util.decode(encodedInput, 2));
        System.out.println("Not decoded: " + Util.decode(encodedInput, -1));
        System.out.println("Not decoded again: " + Util.decode("", 2) + "\n");
        String correctInput = "This is is test`ing Test`ing string. This string is for Test`ing. Don't pay attention.";
        System.out.println("Correct input: " + Arrays.toString(Util.findMostOccurringWords(correctInput)));
        String incorrectInput = "new input new new";
        System.out.println("Incorrect input: " + Arrays.toString(Util.findMostOccurringWords(incorrectInput)));
    }
}
