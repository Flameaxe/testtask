package com.gmail.Flameaxio;

import java.util.*;
import java.util.stream.Collectors;


/**
 * Contains all required methods to solve the task.
 * @author Olexandr Voinalovych
 */
public class Util {
    private Util() {
    }

    /**
     * Takes {@code String} and finds unique words.
     * @param text input String
     * @return Three most occurring words in descending order if there are more than three unique words in {@code String} or empty array if there are not enough unique words.
     */
    public static String[] findMostOccurringWords(String text) {
        List<String> list = Arrays.asList(text.split("\\s+").clone());
        Map<String, Integer> duplicates = countByClassicalLoop(list);
        List<String> mostCommonWords = duplicates.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .map(Map.Entry::getKey)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        if (mostCommonWords.size() > 2) {
            return mostCommonWords.stream().limit(3).toArray(String[]::new);
        }

        return new String[3];
    }

    /**
     * Takes {@code List} and counts how many every word occurs in it.
     * @param inputList List of words.
     * @return Map of words and amount of occurrences respectivly.
     */
    private static Map<String, Integer> countByClassicalLoop(List<String> inputList) {
        Map<String, Integer> resultMap = new LinkedHashMap<>();
        for (String element : inputList) {
            String input = element.replaceAll("[.,]+", "");
            if (resultMap.containsKey(input)) {
                resultMap.put(input, resultMap.get(input) + 1);
            } else {
                resultMap.put(input, 1);
            }
        }
        return resultMap;
    }

    /**
     * Does one iteration of encoding.
     * @param text Input text.
     * @return Encoded text.
     */
    private static String iterateEncode(String text) {
        String input = text.toLowerCase();
        StringBuilder even = new StringBuilder((input.length() + 1) / 2);
        StringBuilder odd = new StringBuilder((input.length() + 1) / 2);
        for (int i = 0; i < input.length(); i++) {
            if ((i + 1) % 2 == 0) {
                even.append(input.charAt(i));
            } else {
                odd.append(input.charAt(i));
            }
        }
        return even.toString() + odd.toString();
    }

    /**
     * Does encoding {@code n} times.
     * @param text Text to be encoded
     * @param n Number of iterations
     * @return Encoded text
     */
    public static String encode(String text, int n) {
        if (text == null || text.length() == 0) {
            return text;
        }
        String result = text.toLowerCase();
        for (int i = 0; i < n; i++) {
            result = iterateEncode(result);
        }
        return result;
    }

    /**
     * Does one iteration of decoding.
     * @param text Input text.
     * @return Decoded text.
     */
    private static String iterateDecode(String text) {
        String firstPart = text.substring(0, text.length() / 2);
        String secondPart = text.substring(text.length() / 2);
        StringBuilder result = new StringBuilder(text.length());
        for (int i = 0; i < text.length() / 2; i++) {
            result.append(secondPart.charAt(i));
            result.append(firstPart.charAt(i));
        }
        return result.toString();
    }

    /**
     * Does decoding {@code n} times.
     * @param encoded_text Text to be encoded
     * @param n Number of iterations
     * @return Encoded text
     */
    public static String decode(String encoded_text, int n) {
        if (encoded_text == null || encoded_text.length() == 0) {
            return encoded_text;
        }
        String result = encoded_text.toLowerCase();
        for (int i = 0; i < n; i++) {
            result = iterateDecode(result);
        }
        return result;
    }
}
